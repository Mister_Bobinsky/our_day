﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScenes : MonoBehaviour
{
    public void SceneLoader(int SceneIndex)
    {
        Debug.Log("LoadScenes Script: Loading Scene #" + SceneIndex);
        
        SceneManager.LoadScene(SceneIndex);
    }
}
