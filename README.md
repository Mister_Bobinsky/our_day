# _Author:_

**Roman Mattia**

# _Game:_

### _Our Day_

# _GitLab:_

https://gitlab.com/Mister_Bobinsky/our_day

# _Works Cited:_

- https://en.wikipedia.org/wiki/Journey_(2012_video_game)
- https://www.youtube.com/watch?v=4-X1FDylROA
- https://www.youtube.com/watch?v=FSt5xrFHaFU
- https://www.youtube.com/watch?v=_QajrabyTJc
- https://www.youtube.com/watch?v=vFvwyu_ZKfU
